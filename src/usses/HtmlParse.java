package usses;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to parse the html page, through jsoup and get the needed information
 * ->EntityCode it stands for code for the each subcategory of products
 * Get the data and start an DownloadManager witch will download all products info.
 */

public class HtmlParse implements Runnable {

    private String Filename;
    private String EntityCode = "none";
    private String Path;
    private int EntitiesNumber = 0;
    private int pageNumber = 0;


    private Document Filetext = null;

    public HtmlParse(String filename, String Path) {
        this.Filename = filename;
        this.Path = Path;
    }

    public void run() {

        String text = null;

        try {
            text = new String(Files.readAllBytes(Paths.get(Filename)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Filetext = Jsoup.parse(text);


        generateEntityCode();
        generateEntitiesNumber();


        if (!EntityCode.equals("none") && pageNumber != 0 && EntitiesNumber != 0) {
            new DownloadManager(getEntityCode(), Path).DownloadData();

        } else {
            DownloadContent.ThreadsRunning--;

        }

    }

    private void generateEntityCode() {

        if (Filetext != null) {

            Element fullCodeText = Filetext.select("[name~=FV]").first();

            Pattern pattern = Pattern.compile(".*\"(.*?)\"");

            if (fullCodeText != null) {
                Matcher codeMatcher = pattern.matcher(fullCodeText.toString());

                if (codeMatcher.find()) {
                    setEntityCode(codeMatcher.group(1));
                }
            }


        }
    }

    private void generateEntitiesNumber() {

        Element fullCodeTotalEntitiesNumber = Filetext.select("[id~=matching-records-count]").first();

        Pattern pattern = Pattern.compile(">(.*?)<");

        if (fullCodeTotalEntitiesNumber != null) {
            Matcher codeMatcher = pattern.matcher(fullCodeTotalEntitiesNumber.toString());

            if (codeMatcher.find()) {
                String tempNumber = codeMatcher.group(1);
                tempNumber = tempNumber.replace(",", "");
                if (tempNumber != "") {
                    setEntitiesNumber(tempNumber);
                }
            }
            setPageNumber();
        }
    }


    public void setEntityCode(String code) {
        EntityCode = code;
    }

    public void setEntitiesNumber(String number) {
        try {
            EntitiesNumber = Integer.parseInt(number);
        } catch (NumberFormatException e) {
        }
    }

    public void setPageNumber() {
        this.pageNumber = (int) Math.ceil((double) getEntityNumber() / 500);
    }

    public String getEntityCode() {
        return EntityCode;
    }

    public int getEntityNumber() {
        return EntitiesNumber;
    }

    public int getpageNumber() {
        return pageNumber;
    }


}
