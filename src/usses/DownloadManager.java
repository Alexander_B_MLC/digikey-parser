package usses;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


/**
 * This class is used to download all csv files with products from each received html file
 * It generate download links based on download link formula for each page of products (500 products per page/csv file)
 * Work way:
 * receive code,page number
 * ->generate download link
 * ->generate path where to save file
 * ->rename it into page+"number page".csv
 */


public class DownloadManager {

    private String enCode;
    private String path;
    private String targetPath;


    public DownloadManager(String code, String path) {
        this.enCode = code;
        this.path = path;


        /**
         * Creating new directory \content for csv files, using the path to the html file
         */

        this.path = path.replaceAll("\\w+.html", "") + "content" + "/";

        this.targetPath = path.replaceAll("\\w+.html", "") + "content" + "/";
        targetPath = "./DigikeyProducts" + targetPath.replaceAll("\\.", "");

        new File(this.path).mkdir();
        new File(this.targetPath).mkdirs();

        new File(this.path + "newProducts" + "/").mkdir();
        new File(this.targetPath + "newProducts" + "/").mkdir();
    }


    public void DownloadData() {
        downloadProducts();
        downloadNewProducts();
        DownloadContent.ThreadsRunning--;

    }


    public void downloadProducts() {

        /**
         * Loop for downloading newProducts.It will continue to download page by page,
         * without knowing the pageNumber, until it will download an emptyFile
         */

        int newPage = 0;

        try {
            do {

                newPage++;
                URL website = urlMaker(enCode, newPage);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());

                System.out.println("Now Coppying " + pathMaker(newPage));
                FileOutputStream fos = new FileOutputStream(pathMaker(newPage));
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                fos.close();

            } while ((new File(pathMaker(newPage)).length() / 1024) > 3);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            /**
             * Everytime last csv file will be an empty one, must delete it
             */

            File mustDeleteFile = new File(pathMaker(newPage));
            if ((mustDeleteFile.length() / 1024) < 3) {
                mustDeleteFile.delete();
            }
        }


    }


    public void downloadNewProducts() {
        /**
         * Loop for downloading newProducts.It will continue to download page by page,
         * without knowing the pageNumber, until it will download an emptyFile
         */

        int newPage = 0;
        try {
            do {

                newPage++;
                URL website = newProductsUrlMaker(enCode, newPage);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());

                System.out.println("Now Coppying " + newProductsPathMaker(newPage));
                FileOutputStream fos = new FileOutputStream(newProductsPathMaker(newPage));
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                fos.close();

            } while ((new File(newProductsPathMaker(newPage)).length() / 1024) > 3);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            /**
             * Everytime last csv file will be an empty one, must delete it
             */

            File mustDeleteFile = new File(newProductsPathMaker(newPage));
            if ((mustDeleteFile.length() / 1024) < 3) {
                mustDeleteFile.delete();
            }

        }


    }


    public URL urlMaker(String EntityCode, int page) throws MalformedURLException {

        String tempPartUrl1 = "https://www.digikey.com/product-search/download.csv?FV=";
        String tempPartUrl2 = "&quantity=0&ColumnSort=0&page=";
        String tempPartUrl3 = "&pageSize=500";

        return new URL(tempPartUrl1 + EntityCode + tempPartUrl2 + Integer.toString(page) + tempPartUrl3);
    }

    public URL newProductsUrlMaker(String EntityCode, int page) throws MalformedURLException {

        String tempPartUrl1 = "https://www.digikey.com/product-search/download.csv?FV=";
        String tempPartUrl2 = "&quantity=0&ColumnSort=0&page=";
        String tempPartUrl3 = "&newproducts=1";
        String tempPartUrl4 = "&pageSize=500";

        return new URL(tempPartUrl1 + EntityCode + tempPartUrl2 + Integer.toString(page) + tempPartUrl3 + tempPartUrl4);

    }

    public String pathMaker(int i) {
        return targetPath + "page" + Integer.toString(i) + ".csv";
    }

    public String newProductsPathMaker(int i) {
        return targetPath + "newProducts" + "/" + "page" + Integer.toString(i) + ".csv";
    }


}
