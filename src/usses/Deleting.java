package usses;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;

public class Deleting {

    public static void main(String[] args) {

        String[] extensions = new String[]{"html"};
        File file = new File("./out/Digikey/products/en");
        Collection<File> files = FileUtils.listFiles(file, extensions, true);
        System.out.println(files.size());


        for (File file2 : files) {
            if ((file2.length() / 1024) < 6) {
                file2.delete();
                System.out.println("delete "+ file2.getPath());
            }
        }

    }

}

