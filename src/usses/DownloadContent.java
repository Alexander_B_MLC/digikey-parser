package usses;

import org.apache.commons.io.FileUtils;

import java.io.File;

import java.util.Collection;

/**
 * The main class that start downloading all csv files with products.
 * It gets all unique .html files from the out directory of spider
 * and starts a thread per each .html files that will parse it for entity code.
 */

public class DownloadContent implements Runnable {
    static int ThreadsRunning = 0;
    final int THREAD_NUMBER = 30;


    public DownloadContent() {
    }


    @Override
    public void run() {

        finishStatement();

        String[] extensions = new String[]{"html"};


        File htmlPath = new File("./out/Digikey/products/en");
        Collection<File> allHtmlFiles = FileUtils.listFiles(htmlPath, extensions, true);



        String prevParent = htmlPath.toString();



        for (File HtmlFile : allHtmlFiles) {

            /**
             * Getting only 1 file per each subcategory, as all html files from a subcategory
             * have the same entity Number we need.
             */

            if (!prevParent.equals(HtmlFile.getParent())) {


                while (getFreeThread() == false) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                new Thread(new HtmlParse(HtmlFile.toString(), HtmlFile.getPath())).start();
                ThreadsRunning++;


                /**
                 * If THREAD_NUMBER is reached , wait until 1 thread is released.
                 */
                try {
                    Thread.sleep(500);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                prevParent = HtmlFile.getParent();
            }

        }


    }

    public void finishStatement() {
        /**
         * Total number of products and number of csv files downloaded
         */
        File file = new File("./DigikeyProducts/out/Digikey/products/en");
        String[] extensions2 = new String[]{"csv"};
        Collection<File> files2 = FileUtils.listFiles(file, extensions2, true);

        System.out.println(files2.size() * 500 + "  Products downloaded = " + files2.size() + " csv Files 500 products each");

    }

    public boolean getFreeThread() {
        return ThreadsRunning < THREAD_NUMBER;
    }


}
